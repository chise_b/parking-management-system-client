package parking;

import parking.utils.StageManager;
import parking.utils.WindowNames;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        StageManager stageManager = StageManager.getInstance();
        stageManager.setPrincipalWindow(primaryStage);
        stageManager.switchScene(StageManager.getLoginView(), WindowNames.LOGIN.getWindowName());
    }


    public static void main(String[] args) {
        launch(args);
    }
}
