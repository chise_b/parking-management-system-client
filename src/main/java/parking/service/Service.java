package parking.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Response;
import parking.model.ParkingSpot;
import parking.networking.NetworkingClient;
import parking.networking.Session;
import parking.networking.request.LocationRequest;
import parking.networking.request.LoginRequest;
import parking.networking.request.RegisterRequest;
import parking.networking.request.ReserveSpotRequest;
import parking.service.exception.UnableToLoginException;
import java.lang.reflect.Type;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Service {

    private static Service INSTANCE = null;
    private static final NetworkingClient client = NetworkingClient.getInstance();
    private static final Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();

    private Service() {
    }

    public static Service getInstance() {
        if (INSTANCE == null) {
            return new Service();
        }
        return INSTANCE;
    }

    public List<ParkingSpot> getAllParkingSpotsDetailsFromALocation() throws IOException {
        List<ParkingSpot> parkingSpots  = new ArrayList<>();
        LocationRequest location = new LocationRequest("str. Nicolae Iorga", "Tora1");
        Response response = client.doSyncPost(NetworkingClient.getBaseUrl() + "/location/spotsdetails", gson.toJson(location));
        if (response.isSuccessful()){
            Type listType = new TypeToken<ArrayList<ParkingSpot>>(){}.getType();
            parkingSpots = gson.fromJson(response.body().string(), listType);
        }
        return parkingSpots;
    }

    public void login(String carNumber, String password) throws IOException, UnableToLoginException {
        LoginRequest loginRequest = new LoginRequest(carNumber, password);
        Response response = client.doSyncPost(NetworkingClient.getBaseUrl() + "/user/login", gson.toJson(loginRequest));
        if(response.isSuccessful()){
            Session deserializedSession = gson.fromJson(response.body().string(), Session.class);
            Session.createSeesion(deserializedSession);
        }
        else{
            throw new UnableToLoginException(response.body().string());
        }
    }

    public void signup(String carNumber, String password, String firstName, String lastName, Integer seniority) throws IOException {
        RegisterRequest registerRequest = new RegisterRequest(carNumber, password, firstName, lastName, seniority);
        Response response = client.doSyncPost(NetworkingClient.getBaseUrl() + "/user/register", gson.toJson(registerRequest));
        if(response.isSuccessful()){
            Session session = new Session(carNumber, firstName, lastName, seniority);
            Session.createSeesion(session);
        }
    }

    public void reserveSpot(int spotNumber, String startOccupyingDateString, String endOccupyingDateString) throws IOException {
        LocationRequest location = new LocationRequest("str. Nicolae Iorga", "Tora1");
        Session userSeesion = Session.getInstance();
        try {
            Date startOccupyingDate =new SimpleDateFormat("dd-MM-yyyy").parse(startOccupyingDateString);
            Date endOccupyingDate =new SimpleDateFormat("dd-MM-yyyy").parse(endOccupyingDateString);
            ReserveSpotRequest reserveSpotRequest = new ReserveSpotRequest(location, userSeesion, startOccupyingDate, endOccupyingDate);
            Response response = client.doSyncPost(NetworkingClient.getBaseUrl() + "/parkingspot/reserve/" + spotNumber, gson.toJson(reserveSpotRequest));
            if(response.isSuccessful()){
                System.out.println("ok");
            }
            else {
                System.out.println("BAD");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
