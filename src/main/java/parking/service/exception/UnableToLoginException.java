package parking.service.exception;

public class UnableToLoginException extends Exception {
    public UnableToLoginException(String message) {
        super(message);
    }
}
