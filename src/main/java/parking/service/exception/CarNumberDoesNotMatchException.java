package parking.service.exception;

public class CarNumberDoesNotMatchException extends Exception {
    public CarNumberDoesNotMatchException(String message) {
        super(message);
    }
}
