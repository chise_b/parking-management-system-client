package parking.service.exception;

public class PasswordDoesNotMatchException extends Exception {
    public PasswordDoesNotMatchException(String message) {
        super(message);
    }
}
