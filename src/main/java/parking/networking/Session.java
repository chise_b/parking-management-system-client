package parking.networking;

public class Session {

    private String carNumber;
    private String firstName;
    private String lastName;
    private Integer seniority;
    private static Session SESSION = null;

    public Session(String carNumber, String firstName, String lastName, Integer seniority) {
        this.carNumber = carNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.seniority = seniority;
    }

    private Session(){}

    public static Session createSeesion(Session deserializedJSONSession){
        if(SESSION == null){
            SESSION = deserializedJSONSession;
        }
        return SESSION;
    }

    public static Session getInstance(){
        return SESSION;
    }

    public static void destroySession() {
        SESSION = null;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSeniority() {
        return seniority;
    }

    public void setSeniority(Integer seniority) {
        this.seniority = seniority;
    }
}
