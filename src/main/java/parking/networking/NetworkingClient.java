package parking.networking;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class NetworkingClient {

    private static NetworkingClient INSTANCE = null;
    private static final String BASE_URL = "http://localhost:8080/api";
    private static final String META_TYPE_JSON_APP = "application/json; charset=utf-8";
    private final OkHttpClient client = new OkHttpClient();

    private NetworkingClient() {
    }

    public static NetworkingClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NetworkingClient();
        }
        return INSTANCE;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public Response doSyncPost(String URL, String JSON) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse(META_TYPE_JSON_APP), JSON);
        Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .build();

        return client.newCall(request).execute();
    }
}
