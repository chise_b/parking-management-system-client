package parking.networking.request;

public class LoginRequest {

    private String carNumber;
    private String password;

    public LoginRequest(String carNumber, String password) {
        this.carNumber = carNumber;
        this.password = password;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getPassword() {
        return password;
    }
}
