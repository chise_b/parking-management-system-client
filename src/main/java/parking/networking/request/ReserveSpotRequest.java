package parking.networking.request;

import parking.networking.Session;

import java.util.Date;

public class ReserveSpotRequest {

    private LocationRequest location;
    private Session parkingUser;
    private Date startOccupyingDate;
    private Date endOccupyingDate;

    public ReserveSpotRequest(LocationRequest location, Session parkingUser, Date startOccupyingDate, Date endOccupyingDate) {
        this.location = location;
        this.parkingUser = parkingUser;
        this.startOccupyingDate = startOccupyingDate;
        this.endOccupyingDate = endOccupyingDate;
    }
}
