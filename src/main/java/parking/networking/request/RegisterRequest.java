package parking.networking.request;

public class RegisterRequest {

    private String carNumber;
    private String password;
    private String firstName;
    private String lastName;
    private Integer seniority;

    public RegisterRequest(String carNumber, String password, String firstName, String lastName, Integer seniority) {
        this.carNumber = carNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.seniority = seniority;
    }
}
