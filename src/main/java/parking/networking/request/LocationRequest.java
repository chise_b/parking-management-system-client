package parking.networking.request;

public class LocationRequest {

    private String locationName;
    private String address;

    public LocationRequest(String address, String locationName) {
        this.locationName = locationName;
        this.address = address;
    }
}
