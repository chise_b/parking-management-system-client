package parking.model;

public class ParkingUser {

    private String carNumber;
    private String firstName;
    private String lastName;
    private Integer seniority;

    public ParkingUser(String carNumber, String firstName, String lastName, Integer seniority) {
        this.carNumber = carNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.seniority = seniority;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSeniority() {
        return seniority;
    }

    public void setSeniority(Integer seniority) {
        this.seniority = seniority;
    }
}
