package parking.model;

import java.util.Date;

public class ParkingSpot {

    private Integer idParkingSpot;
    private Integer spotNumber;
    private Boolean temporaryAvailable;
    private Date startOccupyingDate;
    private Date endOccupyingDate;
    private Date startDateTemporaryAvailability;
    private Date endDateTemporaryAvailability;
    private Boolean available;
    private ParkingSpotType parkingSpotType;
    private ParkingUser parkingSpotUser;

    public ParkingSpot(Integer idParkingSpot, Integer spotNumber, Boolean temporaryAvailable, Date startOccupyingDate, Date endOccupyingDate, Date startDateTemporaryAvailability, Date endDateTemporaryAvailability, Boolean available, ParkingSpotType parkingSpotType, ParkingUser parkingSpotUser) {
        this.idParkingSpot = idParkingSpot;
        this.spotNumber = spotNumber;
        this.temporaryAvailable = temporaryAvailable;
        this.startOccupyingDate = startOccupyingDate;
        this.endOccupyingDate = endOccupyingDate;
        this.startDateTemporaryAvailability = startDateTemporaryAvailability;
        this.endDateTemporaryAvailability = endDateTemporaryAvailability;
        this.available = available;
        this.parkingSpotType = parkingSpotType;
        this.parkingSpotUser = parkingSpotUser;
    }

    public Integer getIdParkingSpot() {
        return idParkingSpot;
    }

    public void setIdParkingSpot(Integer idParkingSpot) {
        this.idParkingSpot = idParkingSpot;
    }

    public Integer getSpotNumber() {
        return spotNumber;
    }

    public void setSpotNumber(Integer spotNumber) {
        this.spotNumber = spotNumber;
    }

    public Boolean getTemporaryAvailable() {
        return temporaryAvailable;
    }

    public void setTemporaryAvailable(Boolean temporaryAvailable) {
        this.temporaryAvailable = temporaryAvailable;
    }

    public Date getStartOccupyingDate() {
        return startOccupyingDate;
    }

    public void setStartOccupyingDate(Date startOccupyingDate) {
        this.startOccupyingDate = startOccupyingDate;
    }

    public Date getEndOccupyingDate() {
        return endOccupyingDate;
    }

    public void setEndOccupyingDate(Date endOccupyingDate) {
        this.endOccupyingDate = endOccupyingDate;
    }

    public Date getStartDateTemporaryAvailability() {
        return startDateTemporaryAvailability;
    }

    public void setStartDateTemporaryAvailability(Date startDateTemporaryAvailability) {
        this.startDateTemporaryAvailability = startDateTemporaryAvailability;
    }

    public Date getEndDateTemporaryAvailability() {
        return endDateTemporaryAvailability;
    }

    public void setEndDateTemporaryAvailability(Date endDateTemporaryAvailability) {
        this.endDateTemporaryAvailability = endDateTemporaryAvailability;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public ParkingSpotType getParkingSpotType() {
        return parkingSpotType;
    }

    public void setParkingSpotType(ParkingSpotType parkingSpotType) {
        this.parkingSpotType = parkingSpotType;
    }

    public ParkingUser getParkingSpotUser() {
        return parkingSpotUser;
    }

    public void setParkingSpotUser(ParkingUser parkingSpotUser) {
        this.parkingSpotUser = parkingSpotUser;
    }
}
