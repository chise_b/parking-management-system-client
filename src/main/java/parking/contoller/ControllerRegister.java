package parking.contoller;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import parking.service.Service;
import parking.service.exception.CarNumberDoesNotMatchException;
import parking.service.exception.PasswordDoesNotMatchException;
import parking.utils.StageManager;
import parking.utils.WindowNames;

import java.io.IOException;

public class ControllerRegister {

    private static final StageManager stageManager = StageManager.getInstance();
    private static final Service service = Service.getInstance();

    @FXML
    private TextField textFieldCarNumber;

    @FXML
    private TextField textFieldCarNumberAgain;

    @FXML
    private TextField textFieldFirstName;

    @FXML
    private TextField textFieldLastName;

    @FXML
    private TextField textFieldSeniority;

    @FXML
    private PasswordField passwordFieldPassword;

    @FXML
    private PasswordField passwordFieldPasswordAgain;

    public void hyperlinkLoginOnClick() {
        try {
            stageManager.switchScene(StageManager.getLoginView(), WindowNames.REGISTER.getWindowName());
        } catch (IOException e) {
            AlertDialog.createAlertDialogBox(Alert.AlertType.ERROR, "UnexpectedError", "There was an unexpected error!", "Contact the admin!\n" + e.getMessage());
        }
    }

    public void buttonSignupOnClick() {
        try {
            checkRegisterRightData();
            String carNumber = textFieldCarNumber.getText();
            String password = passwordFieldPassword.getText();
            String firstName = textFieldFirstName.getText();
            String lastName = textFieldLastName.getText();
            Integer seniority = Integer.parseInt(textFieldSeniority.getText());
            service.signup(carNumber, password, firstName, lastName, seniority);
            stageManager.switchScene(StageManager.getMainWindowView(), WindowNames.MAIN_WINDOW.getWindowName());
        } catch (IOException e) {
            Alert alert = AlertDialog.createAlertDialogBox(Alert.AlertType.ERROR, "UnexpectedError", "There was an unexpected error!", "Contact the admin!\n" + e.getMessage());
            alert.showAndWait();
        } catch (PasswordDoesNotMatchException e) {
            Alert alert = AlertDialog.createAlertDialogBox(Alert.AlertType.ERROR, "Password Error", "Passwords does not match", "Please check your passwords again!");
            alert.showAndWait();
        } catch (CarNumberDoesNotMatchException e) {
            Alert alert = AlertDialog.createAlertDialogBox(Alert.AlertType.ERROR, "Car Number Error", "Car numbers does not match", "Please check your car number again!");
            alert.showAndWait();
        }
    }

    private void checkRegisterRightData() throws PasswordDoesNotMatchException, CarNumberDoesNotMatchException {
        String password = passwordFieldPassword.getText();
        String passwordAgain = passwordFieldPasswordAgain.getText();
        if (!password.equals(passwordAgain) || password == null || passwordAgain == null || password.equals("") || passwordAgain.equals("")) {
            throw new PasswordDoesNotMatchException("Passwords does not match!");
        }
        String carNumber = textFieldCarNumber.getText();
        String carNumberAgain = textFieldCarNumberAgain.getText();
        if (!carNumber.equals(carNumberAgain)) {
            throw new CarNumberDoesNotMatchException("Car number does not match");
        }
    }
}
