package parking.contoller;

import javafx.scene.control.Alert;

public class AlertDialog {

    public static Alert createAlertDialogBox(Alert.AlertType alertType, String title, String headerMessage, String content){
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerMessage);
        alert.setContentText(content);
        return alert;
    }
}
