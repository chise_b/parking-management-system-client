package parking.contoller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import parking.service.Service;
import parking.service.exception.UnableToLoginException;
import parking.utils.StageManager;
import parking.utils.WindowNames;

import java.io.IOException;

public class ControllerLogin {

    private static final StageManager stageManager = StageManager.getInstance();
    private static final Service service = Service.getInstance();

    @FXML
    private TextField textFieldCarNumber;

    @FXML
    private PasswordField passwordFieldPassword;

    @FXML
    public void buttonLoginOnAction() {
        try {
            String carNumber = textFieldCarNumber.getText();
            String password = passwordFieldPassword.getText();
            service.login(carNumber, password);
            stageManager.switchScene(StageManager.getMainWindowView(), WindowNames.MAIN_WINDOW.getWindowName());
        } catch (IOException | UnableToLoginException e) {
            Alert alert = AlertDialog.createAlertDialogBox(Alert.AlertType.ERROR, "Login Error", "Unable To Login", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    public void hyperlinkLoginOnClick() {
        try {
            stageManager.switchScene(StageManager.getRegisterView(), WindowNames.REGISTER.getWindowName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
