package parking.contoller;

import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import parking.model.ParkingSpot;
import parking.networking.Session;
import parking.service.Service;
import parking.utils.StageManager;
import parking.utils.WindowNames;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ControllerMainWindow {

    private static final StageManager stageManager = StageManager.getInstance();
    private static final Service service = Service.getInstance();
    private static Image PARKED_CAR;
    private static Image PARKED_CAR_ROTATED;
    private static final Integer MAXIMUM_CARS_ON_A_ROW = 6;
    private static final Integer CAR_WIDTH = 100;
    private static final Integer CAR_HEIGHT = 150;
    private static final Integer LINE_THICKNES = 10;
    private static final Integer CAR_MARGIN = 10;
    private static final Integer BOTTOM_LINE_MARGIN = 10;
    private static final Integer RECTAGLE_DIFFERENCE = 20;
    public static Integer selectedIdParkingSpot;

    @FXML
    private Pane paneParkingLot;

    @FXML
    public void initialize() {
        PARKED_CAR = new Image(ControllerMainWindow.class.getClassLoader().getResourceAsStream("cars/car_above.jpeg"));
        PARKED_CAR_ROTATED = new Image(ControllerMainWindow.class.getClassLoader().getResourceAsStream("cars/car_above_rotated.jpeg"));
        paneParkingLot.getChildren().clear();
        intializeParkingLot();
    }

    @FXML
    public void buttonLogoutOnClick(){
        Session.destroySession();
        try {
            stageManager.switchScene(StageManager.getLoginView(), WindowNames.LOGIN.getWindowName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ImageView createImageViewParkedCar(int row, int carNumber) {
        ImageView imageViewParkedCar = null;
        if (row % 2 == 0) {
            imageViewParkedCar = new ImageView(PARKED_CAR);
            imageViewParkedCar.setY(row * CAR_HEIGHT + CAR_MARGIN);
        } else {
            imageViewParkedCar = new ImageView(PARKED_CAR_ROTATED);
            imageViewParkedCar.setY(row * CAR_HEIGHT + CAR_MARGIN + 2 * BOTTOM_LINE_MARGIN);
        }
        imageViewParkedCar.setFitHeight(CAR_HEIGHT);
        imageViewParkedCar.setFitWidth(CAR_WIDTH);
        imageViewParkedCar.setPreserveRatio(true);
        imageViewParkedCar.setX(carNumber * CAR_WIDTH + LINE_THICKNES + CAR_MARGIN);

        return imageViewParkedCar;
    }

    private void setLinesThicknessAndColor(Line line) {
        line.setStrokeWidth(LINE_THICKNES);
        line.setStroke(Color.WHITE);
    }

    private Line createMarginLine(int row, int carNumber) {
        Line marginLine = null;
        if (row > 1) {
            marginLine = new Line(carNumber * CAR_WIDTH, row * CAR_HEIGHT, carNumber * CAR_WIDTH, (row + 1) * CAR_HEIGHT + BOTTOM_LINE_MARGIN);
        } else {
            marginLine = new Line(carNumber * CAR_WIDTH, row * CAR_HEIGHT + CAR_MARGIN + 2 * BOTTOM_LINE_MARGIN, carNumber * CAR_WIDTH, (row + 1) * CAR_HEIGHT + CAR_MARGIN + BOTTOM_LINE_MARGIN);
        }
        return marginLine;
    }

    private List<ParkingSpot> getAllSpotsDetails(){
        List<ParkingSpot> allParkingSpots = new ArrayList<>();
        try {
            allParkingSpots = service.getAllParkingSpotsDetailsFromALocation();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allParkingSpots;
    }

    private ParkingSpot findParkingSpotWithSpecificNumber(Integer parkingSpotNumber, List<ParkingSpot> allParkingSpots){
        return allParkingSpots.stream().filter(parkingSpot -> parkingSpot.getSpotNumber() == parkingSpotNumber).findFirst().get();
    }

    private void intializeParkingLot() {
        List<ImageView> images = new ArrayList<>();
        List<Line> lines = new ArrayList<>();
        List<Rectangle> rectangles = new ArrayList<>();
        List<ParkingSpot> allParkigSpots = getAllSpotsDetails();
        int carNumber = 1;
        int row = 0;
        int numberOfCars = allParkigSpots.size();
        while (carNumber <= numberOfCars) {
            for (int carNumberInARow = 0; carNumberInARow < MAXIMUM_CARS_ON_A_ROW && carNumber <= numberOfCars; carNumberInARow++) {
                ParkingSpot parkingSpot = findParkingSpotWithSpecificNumber(carNumber, allParkigSpots);
                if(!parkingSpot.getAvailable()){
                    ImageView parkedCar = createImageViewParkedCar(row, carNumberInARow);
                    images.add(parkedCar);
                }
                else{
                    rectangles.add(createRectangle(row, carNumberInARow, parkingSpot.getSpotNumber()));
                }
                Line marginLine = createMarginLine(row, carNumberInARow);
                Line bottomLine = new Line(carNumberInARow * CAR_WIDTH, CAR_HEIGHT + CAR_MARGIN + BOTTOM_LINE_MARGIN, carNumberInARow * CAR_WIDTH + CAR_WIDTH, CAR_HEIGHT + CAR_MARGIN + BOTTOM_LINE_MARGIN);
                setLinesThicknessAndColor(marginLine);
                setLinesThicknessAndColor(bottomLine);
                lines.add(marginLine);
                lines.add(bottomLine);
                carNumber++;
            }
            row++;
        }
        if (numberOfCars % MAXIMUM_CARS_ON_A_ROW != 0) {
            lines.add(createRemainingLine(numberOfCars, row));
        }
        paneParkingLot.getChildren().addAll(images);
        paneParkingLot.getChildren().addAll(lines);
        paneParkingLot.getChildren().addAll(rectangles);
    }

    private Rectangle createRectangle(int row, int carNumber, Integer parkingSpotNumber) {
        Rectangle rectangle = null;
        if (row >= 1) {
            rectangle = new Rectangle(carNumber * CAR_WIDTH + CAR_MARGIN, row * CAR_HEIGHT + CAR_MARGIN + BOTTOM_LINE_MARGIN + 10, CAR_WIDTH - RECTAGLE_DIFFERENCE, CAR_HEIGHT);
            createRectangeHoverProperty(rectangle);
            setOnMouseClickedRectangle(rectangle, parkingSpotNumber);
        } else {
            rectangle = new Rectangle(carNumber * CAR_WIDTH + CAR_MARGIN, row * CAR_HEIGHT + CAR_MARGIN, CAR_WIDTH - RECTAGLE_DIFFERENCE, CAR_HEIGHT);
            createRectangeHoverProperty(rectangle);
            setOnMouseClickedRectangle(rectangle, parkingSpotNumber);
        }
        rectangle.setFill(Color.web("#F4F4F4"));
        return rectangle;
    }

    private void setOnMouseClickedRectangle(Rectangle rectangle, Integer idParkingSpot){
        rectangle.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent t) {
                selectedIdParkingSpot = idParkingSpot;
                try {
                    stageManager.switchScene(StageManager.getReserveSpotView(), WindowNames.RESERVE_SPOT.getWindowName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void createRectangeHoverProperty(Rectangle rectangle) {
        rectangle.hoverProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean show) -> {
            if(show){
                rectangle.setFill(Color.BLACK);
            }
            else{
                rectangle.setFill(Color.web("#F4F4F4"));
            }
        });

    }

    private Line createRemainingLine(int numberOfCars, int row) {
        int remainigRow = 10 % MAXIMUM_CARS_ON_A_ROW;
        int lastRow = row - 1;
        Line marginLine = new Line(remainigRow * CAR_WIDTH, lastRow * CAR_HEIGHT + CAR_MARGIN + 2 * BOTTOM_LINE_MARGIN, remainigRow * CAR_WIDTH, (lastRow + 1) * CAR_HEIGHT + CAR_MARGIN + BOTTOM_LINE_MARGIN);
        setLinesThicknessAndColor(marginLine);

        return marginLine;
    }

}
