package parking.contoller;


import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import parking.service.Service;
import parking.utils.StageManager;
import parking.utils.WindowNames;

import java.io.IOException;

public class ControllerReserveSpot {

    private static final StageManager stageManager = StageManager.getInstance();
    private static final Service service = Service.getInstance();

    @FXML
    private TextField textFieldSpotNumber;

    @FXML
    private TextField textFieldEndDate;

    @FXML
    private TextField textFieldStartDate;

    @FXML
    public void initialize(){
        textFieldSpotNumber.setText(ControllerMainWindow.selectedIdParkingSpot.toString());
        textFieldSpotNumber.setDisable(true);
    }

    public void buttonCancelOnClick(){
        try {
            stageManager.switchScene(StageManager.getMainWindowView(), WindowNames.MAIN_WINDOW.getWindowName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void buttonReserveOnClick(){
        try {
            service.reserveSpot(Integer.parseInt(textFieldSpotNumber.getText()), textFieldStartDate.getText(), textFieldEndDate.getText());
            stageManager.switchScene(StageManager.getMainWindowView(), WindowNames.MAIN_WINDOW.getWindowName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
