package parking.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class StageManager {

    private static StageManager instance = null;
    private static Stage mainWindow;

    public static synchronized StageManager getInstance() {
        if (instance == null) {
            instance = new StageManager();
        }
        return instance;
    }

    private StageManager() {
    }

    public void setPrincipalWindow(Stage window) {
        mainWindow = window;
    }

    public void switchScene(Parent controller, String windowName) {
        mainWindow.setTitle(windowName);
        mainWindow.setScene(new Scene(controller, 800, 600));
        mainWindow.show();
    }

    public static Parent getLoginView() throws IOException {
        return new FXMLLoader().load(StageManager.class.getClassLoader().getResourceAsStream("views/Login.fxml"));
    }

    public static Parent getReserveSpotView() throws IOException {
        return new FXMLLoader().load(StageManager.class.getClassLoader().getResourceAsStream("views/ReserveSpot.fxml"));
    }

    public static Parent getRegisterView() throws IOException {
        return new FXMLLoader().load(StageManager.class.getClassLoader().getResourceAsStream("views/Register.fxml"));
    }

    public static Parent getMainWindowView() throws IOException {
        return new FXMLLoader().load(StageManager.class.getClassLoader().getResourceAsStream("views/MainWindow.fxml"));
    }
}
