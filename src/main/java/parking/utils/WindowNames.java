package parking.utils;

public enum WindowNames {

    LOGIN("Login Window"), REGISTER("Register Window"), MAIN_WINDOW("Main Window"), RESERVE_SPOT("Reserve Spot Window");

    private String windowName;

    WindowNames(String windowName){
        this.windowName = windowName;
    }

    public String getWindowName(){
        return this.windowName;
    }
}
